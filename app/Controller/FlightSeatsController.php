<?php
App::uses('AppController', 'Controller');
/**
 * FlightSeats Controller
 *
 * @property FlightSeat $FlightSeat
 * @property PaginatorComponent $Paginator
 */
class FlightSeatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FlightSeat->recursive = 0;
		$this->set('flightSeats', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FlightSeat->exists($id)) {
			throw new NotFoundException(__('Invalid flight seat'));
		}
		$options = array('conditions' => array('FlightSeat.' . $this->FlightSeat->primaryKey => $id));
		$this->set('flightSeat', $this->FlightSeat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FlightSeat->create();
			if ($this->FlightSeat->save($this->request->data)) {
				$this->Session->setFlash(__('The flight seat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The flight seat could not be saved. Please, try again.'));
			}
		}
		$flights = $this->FlightSeat->Flight->find('list');
		$this->set(compact('flights'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FlightSeat->exists($id)) {
			throw new NotFoundException(__('Invalid flight seat'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FlightSeat->save($this->request->data)) {
				$this->Session->setFlash(__('The flight seat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The flight seat could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FlightSeat.' . $this->FlightSeat->primaryKey => $id));
			$this->request->data = $this->FlightSeat->find('first', $options);
		}
		$flights = $this->FlightSeat->Flight->find('list');
		$this->set(compact('flights'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FlightSeat->id = $id;
		if (!$this->FlightSeat->exists()) {
			throw new NotFoundException(__('Invalid flight seat'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FlightSeat->delete()) {
			$this->Session->setFlash(__('The flight seat has been deleted.'));
		} else {
			$this->Session->setFlash(__('The flight seat could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
