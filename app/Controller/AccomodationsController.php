<?php
App::uses('AppController', 'Controller');
/**
 * Accomodations Controller
 *
 * @property Accomodation $Accomodation
 * @property PaginatorComponent $Paginator
 */
class AccomodationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Accomodation->recursive = 0;
		$this->set('accomodations', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Accomodation->exists($id)) {
			throw new NotFoundException(__('Invalid accomodation'));
		}
		$options = array('conditions' => array('Accomodation.' . $this->Accomodation->primaryKey => $id));
		$this->set('accomodation', $this->Accomodation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Accomodation->create();
			if ($this->Accomodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accomodation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accomodation could not be saved. Please, try again.'));
			}
		}
		$cities = $this->Accomodation->City->find('list');
		$this->set(compact('cities'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Accomodation->exists($id)) {
			throw new NotFoundException(__('Invalid accomodation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Accomodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accomodation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accomodation could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Accomodation.' . $this->Accomodation->primaryKey => $id));
			$this->request->data = $this->Accomodation->find('first', $options);
		}
		$cities = $this->Accomodation->City->find('list');
		$this->set(compact('cities'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Accomodation->id = $id;
		if (!$this->Accomodation->exists()) {
			throw new NotFoundException(__('Invalid accomodation'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Accomodation->delete()) {
			$this->Session->setFlash(__('The accomodation has been deleted.'));
		} else {
			$this->Session->setFlash(__('The accomodation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
