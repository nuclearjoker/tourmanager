<?php
App::uses('AppController', 'Controller');
/**
 * ClientFlightSeats Controller
 *
 * @property ClientFlightSeat $ClientFlightSeat
 * @property PaginatorComponent $Paginator
 */
class ClientFlightSeatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ClientFlightSeat->recursive = 0;
		$this->set('clientFlightSeats', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ClientFlightSeat->exists($id)) {
			throw new NotFoundException(__('Invalid client flight seat'));
		}
		$options = array('conditions' => array('ClientFlightSeat.' . $this->ClientFlightSeat->primaryKey => $id));
		$this->set('clientFlightSeat', $this->ClientFlightSeat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ClientFlightSeat->create();
			if ($this->ClientFlightSeat->save($this->request->data)) {
				$this->Session->setFlash(__('The client flight seat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The client flight seat could not be saved. Please, try again.'));
			}
		}
		$clients = $this->ClientFlightSeat->Client->find('list');
		$flightSeats = $this->ClientFlightSeat->FlightSeat->find('list');
		$this->set(compact('clients', 'flightSeats'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ClientFlightSeat->exists($id)) {
			throw new NotFoundException(__('Invalid client flight seat'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ClientFlightSeat->save($this->request->data)) {
				$this->Session->setFlash(__('The client flight seat has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The client flight seat could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ClientFlightSeat.' . $this->ClientFlightSeat->primaryKey => $id));
			$this->request->data = $this->ClientFlightSeat->find('first', $options);
		}
		$clients = $this->ClientFlightSeat->Client->find('list');
		$flightSeats = $this->ClientFlightSeat->FlightSeat->find('list');
		$this->set(compact('clients', 'flightSeats'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ClientFlightSeat->id = $id;
		if (!$this->ClientFlightSeat->exists()) {
			throw new NotFoundException(__('Invalid client flight seat'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ClientFlightSeat->delete()) {
			$this->Session->setFlash(__('The client flight seat has been deleted.'));
		} else {
			$this->Session->setFlash(__('The client flight seat could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
