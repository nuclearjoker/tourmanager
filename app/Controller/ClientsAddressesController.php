<?php
App::uses('AppController', 'Controller');
/**
 * ClientsAddresses Controller
 *
 * @property ClientsAddress $ClientsAddress
 * @property PaginatorComponent $Paginator
 */
class ClientsAddressesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ClientsAddress->recursive = 0;
		$this->set('clientsAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ClientsAddress->exists($id)) {
			throw new NotFoundException(__('Invalid clients address'));
		}
		$options = array('conditions' => array('ClientsAddress.' . $this->ClientsAddress->primaryKey => $id));
		$this->set('clientsAddress', $this->ClientsAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ClientsAddress->create();
			if ($this->ClientsAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The clients address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The clients address could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ClientsAddress->exists($id)) {
			throw new NotFoundException(__('Invalid clients address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ClientsAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The clients address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The clients address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ClientsAddress.' . $this->ClientsAddress->primaryKey => $id));
			$this->request->data = $this->ClientsAddress->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ClientsAddress->id = $id;
		if (!$this->ClientsAddress->exists()) {
			throw new NotFoundException(__('Invalid clients address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ClientsAddress->delete()) {
			$this->Session->setFlash(__('The clients address has been deleted.'));
		} else {
			$this->Session->setFlash(__('The clients address could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
