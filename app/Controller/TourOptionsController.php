<?php
App::uses('AppController', 'Controller');
/**
 * TourOptions Controller
 *
 * @property TourOption $TourOption
 * @property PaginatorComponent $Paginator
 */
class TourOptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TourOption->recursive = 0;
		$this->set('tourOptions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TourOption->exists($id)) {
			throw new NotFoundException(__('Invalid tour option'));
		}
		$options = array('conditions' => array('TourOption.' . $this->TourOption->primaryKey => $id));
		$this->set('tourOption', $this->TourOption->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TourOption->create();
			if ($this->TourOption->save($this->request->data)) {
				$this->Session->setFlash(__('The tour option has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tour option could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TourOption->exists($id)) {
			throw new NotFoundException(__('Invalid tour option'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TourOption->save($this->request->data)) {
				$this->Session->setFlash(__('The tour option has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tour option could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TourOption.' . $this->TourOption->primaryKey => $id));
			$this->request->data = $this->TourOption->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TourOption->id = $id;
		if (!$this->TourOption->exists()) {
			throw new NotFoundException(__('Invalid tour option'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TourOption->delete()) {
			$this->Session->setFlash(__('The tour option has been deleted.'));
		} else {
			$this->Session->setFlash(__('The tour option could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
