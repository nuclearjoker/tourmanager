<?php
App::uses('AppController', 'Controller');


class SearchersController extends AppController {
	public $uses = array('Tour', 'Client');
	public $components = array('RequestHandler');
	public function search($term) {
		//$this->RequestHandler->setContent('json', 'application/json' );
		$sortedIds = $this->Client->esSearchGetKeys('*'.$term.'*');
		$results = $this->Client->find('all', array(
    		'conditions' => array(
        		"id" => $sortedIds
    		)
	     ));
		$results = $this->Client->searchResultsResort($results, $sortedIds);
		$this->set(compact('results'));
	}
}