<?php
App::uses('AppController', 'Controller');
/**
 * TourPlans Controller
 *
 * @property TourPlan $TourPlan
 * @property PaginatorComponent $Paginator
 */
class TourPlansController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TourPlan->recursive = 2;
		$this->set('tourPlans', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TourPlan->exists($id)) {
			throw new NotFoundException(__('Invalid tour plan'));
		}
		$options = array('conditions' => array('TourPlan.' . $this->TourPlan->primaryKey => $id));
		$this->set('tourPlan', $this->TourPlan->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($tour_id = null) {
		if ($this->request->is('post')) {
			$this->TourPlan->create();
			if ($this->TourPlan->save($this->request->data)) {
				$this->Session->setFlash(__('The tour plan has been saved.'));
				if (!is_numeric($tour_id) ) {
				 	return $this->redirect(array('action' => 'index'));
				} else {
				 	return $this->redirect(array('controller' => 'tours', 'action' => 'view', $tour_id));
				}

			} else {
				$this->Session->setFlash(__('The tour plan could not be saved. Please, try again.'));
				# TODO: check that there is only 1 main plan per tour.
			}
		}
		$this->set('tour_id', $tour_id);
		$tours = $this->TourPlan->Tour->find('list');
		$this->set(compact('tours'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TourPlan->exists($id)) {
			throw new NotFoundException(__('Invalid tour plan'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TourPlan->save($this->request->data)) {
				$this->Session->setFlash(__('The tour plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tour plan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TourPlan.' . $this->TourPlan->primaryKey => $id));
			$this->request->data = $this->TourPlan->find('first', $options);
		}
		$tours = $this->TourPlan->Tour->find('list');
		$this->set(compact('tours'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TourPlan->id = $id;
		if (!$this->TourPlan->exists()) {
			throw new NotFoundException(__('Invalid tour plan'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TourPlan->delete()) {
			$this->Session->setFlash(__('The tour plan has been deleted.'));
		} else {
			$this->Session->setFlash(__('The tour plan could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
