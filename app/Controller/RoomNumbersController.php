<?php
App::uses('AppController', 'Controller');
/**
 * RoomNumbers Controller
 *
 * @property RoomNumber $RoomNumber
 * @property PaginatorComponent $Paginator
 */
class RoomNumbersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RoomNumber->recursive = 0;
		$this->set('roomNumbers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RoomNumber->exists($id)) {
			throw new NotFoundException(__('Invalid room number'));
		}
		$options = array('conditions' => array('RoomNumber.' . $this->RoomNumber->primaryKey => $id));
		$this->set('roomNumber', $this->RoomNumber->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RoomNumber->create();
			if ($this->RoomNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The room number has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The room number could not be saved. Please, try again.'));
			}
		}
		$accomodations = $this->RoomNumber->Accomodation->find('list');
		$this->set(compact('accomodations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RoomNumber->exists($id)) {
			throw new NotFoundException(__('Invalid room number'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RoomNumber->save($this->request->data)) {
				$this->Session->setFlash(__('The room number has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The room number could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RoomNumber.' . $this->RoomNumber->primaryKey => $id));
			$this->request->data = $this->RoomNumber->find('first', $options);
		}
		$accomodations = $this->RoomNumber->Accomodation->find('list');
		$this->set(compact('accomodations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RoomNumber->id = $id;
		if (!$this->RoomNumber->exists()) {
			throw new NotFoundException(__('Invalid room number'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RoomNumber->delete()) {
			$this->Session->setFlash(__('The room number has been deleted.'));
		} else {
			$this->Session->setFlash(__('The room number could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
