<?php
App::uses('AppController', 'Controller');
/**
 * SupplierCategories Controller
 *
 * @property SupplierCategory $SupplierCategory
 * @property PaginatorComponent $Paginator
 */
class SupplierCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SupplierCategory->recursive = 0;
		$this->set('supplierCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SupplierCategory->exists($id)) {
			throw new NotFoundException(__('Invalid supplier category'));
		}
		$options = array('conditions' => array('SupplierCategory.' . $this->SupplierCategory->primaryKey => $id));
		$this->set('supplierCategory', $this->SupplierCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SupplierCategory->create();
			if ($this->SupplierCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The supplier category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The supplier category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SupplierCategory->exists($id)) {
			throw new NotFoundException(__('Invalid supplier category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SupplierCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The supplier category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The supplier category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SupplierCategory.' . $this->SupplierCategory->primaryKey => $id));
			$this->request->data = $this->SupplierCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SupplierCategory->id = $id;
		if (!$this->SupplierCategory->exists()) {
			throw new NotFoundException(__('Invalid supplier category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SupplierCategory->delete()) {
			$this->Session->setFlash(__('The supplier category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The supplier category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
