<?php
App::uses('AppController', 'Controller');
/**
 * Flights Controller
 *
 * @property Flight $Flight
 * @property PaginatorComponent $Paginator
 */
class FlightsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Flight->recursive = 0;
		$this->set('flights', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Flight->exists($id)) {
			throw new NotFoundException(__('Invalid flight'));
		}
		$options = array('conditions' => array('Flight.' . $this->Flight->primaryKey => $id));
		$this->set('flight', $this->Flight->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($tour_id = null) {
		if ($this->request->is('post')) {
			$this->Flight->create();
			if ($this->Flight->save($this->request->data)) {
				$this->Session->setFlash(__('The flight has been saved.'));
				if (!is_numeric($tour_id)) {
				 	return $this->redirect(array('action' => 'index'));
				} else {
				 	return $this->redirect(array('controller' => 'tours', 'action' => 'view', $tour_id));
				}
			} else {
				$this->Session->setFlash(__('The flight could not be saved. Please, try again.'));
			}
		}

		$this->set('tour_id', $tour_id);
		$tours = $this->Flight->Tour->find('list');
		$fromCities = $this->Flight->FromCity->find('list');
		$toCities = $this->Flight->ToCity->find('list');
		$this->set(compact('tours', 'fromCities', 'toCities'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Flight->exists($id)) {
			throw new NotFoundException(__('Invalid flight'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Flight->save($this->request->data)) {
				$this->Session->setFlash(__('The flight has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The flight could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Flight.' . $this->Flight->primaryKey => $id));
			$this->request->data = $this->Flight->find('first', $options);
		}
		$tours = $this->Flight->Tour->find('list');
		$fromCities = $this->Flight->FromCity->find('list');
		$toCities = $this->Flight->ToCity->find('list');
		$this->set(compact('tours', 'fromCities', 'toCities'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Flight->id = $id;
		if (!$this->Flight->exists()) {
			throw new NotFoundException(__('Invalid flight'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Flight->delete()) {
			$this->Session->setFlash(__('The flight has been deleted.'));
		} else {
			$this->Session->setFlash(__('The flight could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
