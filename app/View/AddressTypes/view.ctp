<div class="addressTypes view">
<h2><?php echo __('Address Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($addressType['AddressType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address Type'); ?></dt>
		<dd>
			<?php echo h($addressType['AddressType']['address_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($addressType['AddressType']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Address Type'), array('action' => 'edit', $addressType['AddressType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Address Type'), array('action' => 'delete', $addressType['AddressType']['id']), array(), __('Are you sure you want to delete # %s?', $addressType['AddressType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Address Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
