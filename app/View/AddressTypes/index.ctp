<div class="addressTypes index">
	<h2><?php echo __('Address Types'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('address_type'); ?></th>
    		<th><?php echo $this->Paginator->sort('name'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($addressTypes as $addressType): ?>
	<tr>
		<td><?php echo h($addressType['AddressType']['address_type']); ?>&nbsp;</td>
		<td><?php echo h($addressType['AddressType']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $addressType['AddressType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $addressType['AddressType']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $addressType['AddressType']['id']), array(), __('Are you sure you want to delete # %s?', $addressType['AddressType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Address Type'), array('action' => 'add')); ?></li>
	</ul>
</div>
