<h2><?php echo __('Cities'); ?></h2>
<table cellpadding="0" cellspacing="0">
<thead>
<tr>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('country_id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
</tr>
</thead>
<tbody>
<?php foreach ($cities as $city): ?>
<tr>
	<td><?php echo h($city['City']['name']); ?>&nbsp;</td>
	<td>
		<?php echo $this->Html->link($city['Country']['name'], array('controller' => 'countries', 'action' => 'view', $city['Country']['name'])); ?>
	</td>
	<td class="actions">
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $city['City']['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $city['City']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $city['City']['id']), array(), __('Are you sure you want to delete %s?', $city['City']['name'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php echo $this->element('pagination'); ?>
<?php echo $this->Html->link(__('New City'), array('action' => 'add'), array('class' => 'button tiny')); ?>