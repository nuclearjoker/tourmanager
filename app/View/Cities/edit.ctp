<?php echo $this->Form->create('City'); ?>
	<fieldset>
		<legend><?php echo __('Edit City'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('country_id', array( 'class' => 'select2'));
	?>
	</fieldset>
	<input type="submit" value="Save City" class="button tiny">
<?php echo $this->Form->end(); ?>
</div>