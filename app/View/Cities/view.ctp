<div class="cities view">
<h2><?php echo __('City'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($city['City']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City Name'); ?></dt>
		<dd>
			<?php echo h($city['City']['city_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($city['Country']['id'], array('controller' => 'countries', 'action' => 'view', $city['Country']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit City'), array('action' => 'edit', $city['City']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete City'), array('action' => 'delete', $city['City']['id']), array(), __('Are you sure you want to delete # %s?', $city['City']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accomodations'), array('controller' => 'accomodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accomodation'), array('controller' => 'accomodations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplier Types'), array('controller' => 'supplier_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier Type'), array('controller' => 'supplier_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accomodations'); ?></h3>
	<?php if (!empty($city['Accomodation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($city['Accomodation'] as $accomodation): ?>
		<tr>
			<td><?php echo $accomodation['id']; ?></td>
			<td><?php echo $accomodation['city_id']; ?></td>
			<td><?php echo $accomodation['address']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accomodations', 'action' => 'view', $accomodation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accomodations', 'action' => 'edit', $accomodation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accomodations', 'action' => 'delete', $accomodation['id']), array(), __('Are you sure you want to delete # %s?', $accomodation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Accomodation'), array('controller' => 'accomodations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Supplier Types'); ?></h3>
	<?php if (!empty($city['SupplierType'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Idsupplier Type'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($city['SupplierType'] as $supplierType): ?>
		<tr>
			<td><?php echo $supplierType['idsupplier_type']; ?></td>
			<td><?php echo $supplierType['city_id']; ?></td>
			<td><?php echo $supplierType['name']; ?></td>
			<td><?php echo $supplierType['address']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'supplier_types', 'action' => 'view', $supplierType['idsupplier_type'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'supplier_types', 'action' => 'edit', $supplierType['idsupplier_type'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'supplier_types', 'action' => 'delete', $supplierType['idsupplier_type']), array(), __('Are you sure you want to delete # %s?', $supplierType['idsupplier_type'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Supplier Type'), array('controller' => 'supplier_types', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Suppliers'); ?></h3>
	<?php if (!empty($city['Supplier'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Supplier Category Id'); ?></th>
		<th><?php echo __('Supplier Type Id'); ?></th>
		<th><?php echo __('Alternative Name'); ?></th>
		<th><?php echo __('Short Code'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Contact Person'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($city['Supplier'] as $supplier): ?>
		<tr>
			<td><?php echo $supplier['id']; ?></td>
			<td><?php echo $supplier['supplier_category_id']; ?></td>
			<td><?php echo $supplier['supplier_type_id']; ?></td>
			<td><?php echo $supplier['alternative_name']; ?></td>
			<td><?php echo $supplier['short_code']; ?></td>
			<td><?php echo $supplier['address']; ?></td>
			<td><?php echo $supplier['city_id']; ?></td>
			<td><?php echo $supplier['email']; ?></td>
			<td><?php echo $supplier['contact_person']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'suppliers', 'action' => 'view', $supplier['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'suppliers', 'action' => 'edit', $supplier['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'suppliers', 'action' => 'delete', $supplier['id']), array(), __('Are you sure you want to delete # %s?', $supplier['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Tours'); ?></h3>
	<?php if (!empty($city['Tour'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tour Name'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Date From'); ?></th>
		<th><?php echo __('Date To'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Breakaway'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($city['Tour'] as $tour): ?>
		<tr>
			<td><?php echo $tour['id']; ?></td>
			<td><?php echo $tour['tour_name']; ?></td>
			<td><?php echo $tour['city_id']; ?></td>
			<td><?php echo $tour['date_from']; ?></td>
			<td><?php echo $tour['date_to']; ?></td>
			<td><?php echo $tour['parent_id']; ?></td>
			<td><?php echo $tour['breakaway']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tours', 'action' => 'view', $tour['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tours', 'action' => 'edit', $tour['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tours', 'action' => 'delete', $tour['id']), array(), __('Are you sure you want to delete # %s?', $tour['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
