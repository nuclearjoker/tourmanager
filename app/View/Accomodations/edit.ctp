<?php echo $this->Form->create('Accomodation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Accomodation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('address');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>