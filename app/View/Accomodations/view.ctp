<div class="accomodations view">
<h2><?php echo __('Accomodation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accomodation['Accomodation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accomodation['City']['id'], array('controller' => 'cities', 'action' => 'view', $accomodation['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($accomodation['Accomodation']['address']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accomodation'), array('action' => 'edit', $accomodation['Accomodation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accomodation'), array('action' => 'delete', $accomodation['Accomodation']['id']), array(), __('Are you sure you want to delete # %s?', $accomodation['Accomodation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Accomodations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accomodation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Room Numbers'), array('controller' => 'room_numbers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room Number'), array('controller' => 'room_numbers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Room Numbers'); ?></h3>
	<?php if (!empty($accomodation['RoomNumber'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Accomodation Id'); ?></th>
		<th><?php echo __('Room Code'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accomodation['RoomNumber'] as $roomNumber): ?>
		<tr>
			<td><?php echo $roomNumber['id']; ?></td>
			<td><?php echo $roomNumber['accomodation_id']; ?></td>
			<td><?php echo $roomNumber['room_code']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'room_numbers', 'action' => 'view', $roomNumber['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'room_numbers', 'action' => 'edit', $roomNumber['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'room_numbers', 'action' => 'delete', $roomNumber['id']), array(), __('Are you sure you want to delete # %s?', $roomNumber['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Room Number'), array('controller' => 'room_numbers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
