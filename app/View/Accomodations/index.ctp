	<h2><?php echo __('Accomodations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('city_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('address'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accomodations as $accomodation): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($accomodation['City']['id'], array('controller' => 'cities', 'action' => 'view', $accomodation['City']['id'])); ?>
		</td>
		<td><?php echo h($accomodation['Accomodation']['address']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $accomodation['Accomodation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $accomodation['Accomodation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $accomodation['Accomodation']['id']), array(), __('Are you sure you want to delete # %s?', $accomodation['Accomodation']['id'])); ?>
		</td>
	</tr>
    <?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->element('pagination'); ?>
<?php $this->Html->link(__('New Accomodation'), array('action'=>'add') ); ?>