<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');


		echo $this->fetch('meta');

        echo $this->Html->css('foundation.css');
        echo $this->Html->css('jquery-ui.min.css');
        echo $this->Html->css('metallic.css');
        echo $this->Html->css('select2-foundation5.css');
        echo $this->fetch('css');

        echo $this->Html->script('vendor/jquery.js');
        echo $this->Html->script('jquery-ui.min.js');
        echo $this->Html->script('zebra_datepicker.js');
        echo $this->Html->script('select2.min.js');
        echo $this->Html->script('foundation.min.js');
        echo $this->Html->script('foundation/foundation.topbar.js');
        echo $this->fetch('script');

	?>
	<script>
	$( document ).ready(function () {
		$(".select2").select2();
	});
	</script>
</head>
<body>

<?php echo $this->element('menu'); ?>

<div class="row">
<div class="large-12 columns">
	<?php $flashmsg = $this->Session->flash();
	 if (strlen($flashmsg) > 0) { ?>
		<div data-alert class="alert-box alert">
		  <?php echo $flashmsg; ?>
		  <a href="#" class="close">&times;</a>
		</div>
	<?php } ?>

	<?php echo $this->fetch('content'); ?>
</div>
</div>


<?php echo $this->element('sql_dump'); ?>
<script>$(document).foundation();</script>
</body>
</html>
