<div class="flights view">
<h2><?php echo __('Flight'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($flight['Flight']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Time'); ?></dt>
		<dd>
			<?php echo h($flight['Flight']['departure_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Landing Time'); ?></dt>
		<dd>
			<?php echo h($flight['Flight']['landing_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tour'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flight['Tour']['id'], array('controller' => 'tours', 'action' => 'view', $flight['Tour']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('From City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flight['FromCity']['id'], array('controller' => 'cities', 'action' => 'view', $flight['FromCity']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('To City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flight['ToCity']['id'], array('controller' => 'cities', 'action' => 'view', $flight['ToCity']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Flight'), array('action' => 'edit', $flight['Flight']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Flight'), array('action' => 'delete', $flight['Flight']['id']), array(), __('Are you sure you want to delete # %s?', $flight['Flight']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Flights'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New From City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Flight Seats'); ?></h3>
	<?php if (!empty($flight['FlightSeat'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Flight Id'); ?></th>
		<th><?php echo __('Seat Code'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($flight['FlightSeat'] as $flightSeat): ?>
		<tr>
			<td><?php echo $flightSeat['id']; ?></td>
			<td><?php echo $flightSeat['flight_id']; ?></td>
			<td><?php echo $flightSeat['seat_code']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'flight_seats', 'action' => 'view', $flightSeat['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'flight_seats', 'action' => 'edit', $flightSeat['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'flight_seats', 'action' => 'delete', $flightSeat['id']), array(), __('Are you sure you want to delete # %s?', $flightSeat['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
