<div class="flights form">
<?php echo $this->Form->create('Flight'); ?>
	<fieldset>
		<legend><?php echo __('Edit Flight'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('departure_time');
		echo $this->Form->input('landing_time');
		echo $this->Form->input('tour_id');
		echo $this->Form->input('from_city_id');
		echo $this->Form->input('to_city_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Flight.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Flight.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Flights'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New From City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
