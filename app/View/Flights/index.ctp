<div class="flights index">
	<h2><?php echo __('Flights'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('departure_time'); ?></th>
    		<th><?php echo $this->Paginator->sort('landing_time'); ?></th>
    		<th><?php echo $this->Paginator->sort('tour_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('from_city_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('to_city_id'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($flights as $flight): ?>
	<tr>
		<td><?php echo h($flight['Flight']['departure_time']); ?>&nbsp;</td>
		<td><?php echo h($flight['Flight']['landing_time']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($flight['Tour']['id'], array('controller' => 'tours', 'action' => 'view', $flight['Tour']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($flight['FromCity']['id'], array('controller' => 'cities', 'action' => 'view', $flight['FromCity']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($flight['ToCity']['id'], array('controller' => 'cities', 'action' => 'view', $flight['ToCity']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $flight['Flight']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $flight['Flight']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $flight['Flight']['id']), array(), __('Are you sure you want to delete # %s?', $flight['Flight']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Flight'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New From City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
