<?php echo $this->Form->create('Flight'); ?>
	<fieldset>
		<legend><?php echo __('Add Flight'); ?></legend>
	<?php
		echo $this->Form->input('departure_time');
		echo $this->Form->input('landing_time');
		echo $this->Form->input('tour_id');
		echo $this->Form->input('from_city_id');
		echo $this->Form->input('to_city_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

<?php echo $this->Html->link(__('List Flights'), array('action' => 'index')); ?>
<?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?>
<?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?>
		