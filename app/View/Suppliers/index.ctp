
<h2><?php echo __('Suppliers'); ?></h2>
<table cellpadding="0" cellspacing="0">
<thead>
<tr>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('alternative_name'); ?></th>
		<th><?php echo $this->Paginator->sort('short_code'); ?></th>
		<th><?php echo $this->Paginator->sort('address'); ?></th>
		<th><?php echo $this->Paginator->sort('city_id'); ?></th>
		<th><?php echo $this->Paginator->sort('email'); ?></th>
		<th><?php echo $this->Paginator->sort('contact_person'); ?></th>
		<th><?php echo __('Actions'); ?></th>
</tr>
</thead>
<tbody>
<?php foreach ($suppliers as $supplier): ?>
<tr>
	<td>
		<?php echo $this->Html->link($supplier['SupplierType']['name'], array('controller' => 'supplier_types', 'action' => 'view', $supplier['SupplierType']['id'])); ?>
	</td>
	<td><?php echo h($supplier['Supplier']['alternative_name']); ?>&nbsp;</td>
	<td><?php echo h($supplier['Supplier']['short_code']); ?>&nbsp;</td>
	<td><?php echo h($supplier['Supplier']['address']); ?>&nbsp;</td>
	<td>
		<?php echo $this->Html->link($supplier['City']['name'], array('controller' => 'cities', 'action' => 'view', $supplier['City']['id'])); ?>
	</td>
	<td><?php echo h($supplier['Supplier']['email']); ?>&nbsp;</td>
	<td><?php echo h($supplier['Supplier']['contact_person']); ?>&nbsp;</td>
	<td class="actions">
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $supplier['Supplier']['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $supplier['Supplier']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $supplier['Supplier']['id']), array(), __('Are you sure you want to delete %s?', $supplier['Supplier']['name'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

<?php echo $this->element('pagination'); ?>

<?php echo $this->Html->link(__('New Supplier'), array('action' => 'add'), array('class' => 'button tiny')); ?>
