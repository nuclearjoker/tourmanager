
<h2><?php echo __('Clients'); ?></h2>
<table cellpadding="0" cellspacing="0">
<thead>
<tr>
		<th><?php echo $this->Paginator->sort('first_name'); ?></th>
		<th><?php echo $this->Paginator->sort('last_name'); ?></th>
		<th><?php echo $this->Paginator->sort('id_number'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
</tr>
</thead>
<tbody>
<?php foreach ($clients as $client): ?>
<tr>
	<td><?php echo h($client['Client']['first_name']); ?>&nbsp;</td>
	<td><?php echo h($client['Client']['last_name']); ?>&nbsp;</td>
	<td><?php echo h($client['Client']['id_number']); ?>&nbsp;</td>
	<td class="actions">
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $client['Client']['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $client['Client']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $client['Client']['id']), array(), __('Are you sure you want to delete # %s?', $client['Client']['id'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

<?php echo $this->element('pagination'); ?>


<?php echo $this->Html->link(__('New Client'), array('action' => 'add'), array('class' => 'button tiny')); ?>