
<?php echo $this->Form->create('Client'); ?>
	<fieldset>
		<legend><?php echo __('Add Client'); ?></legend>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('id_number');
		echo $this->Form->input('Address', array( "class" => "select2" ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

<?php echo $this->Html->link(__('List Clients'), array('action' => 'index')); ?>