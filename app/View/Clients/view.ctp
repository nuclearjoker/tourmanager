
<h2><?php echo __('Client'); ?></h2>
	<dl>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($client['Client']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($client['Client']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Id Number'); ?></dt>
		<dd>
			<?php echo h($client['Client']['id_number']); ?>
			&nbsp;
		</dd>
	</dl>
</div>




<h3><?php echo __('Client Flight Seats'); ?></h3>
<?php if (!empty($client['ClientFlightSeat'])): ?>
<table cellpadding = "0" cellspacing = "0">
<tr>
	<th><?php echo __('Client Id'); ?></th>
	<th><?php echo __('Flight Seat Id'); ?></th>
	<th class="actions"><?php echo __('Actions'); ?></th>
</tr>
<?php foreach ($client['ClientFlightSeat'] as $clientFlightSeat): ?>
	<tr>
		<td><?php echo $clientFlightSeat['client_id']; ?></td>
		<td><?php echo $clientFlightSeat['flight_seat_id']; ?></td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('controller' => 'client_flight_seats', 'action' => 'view', $clientFlightSeat['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'client_flight_seats', 'action' => 'edit', $clientFlightSeat['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'client_flight_seats', 'action' => 'delete', $clientFlightSeat['id']), array(), __('Are you sure you want to delete # %s?', $clientFlightSeat['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>


<?php echo $this->Html->link(__('New Client Flight Seat'), array('controller' => 'client_flight_seats', 'action' => 'add'), array( 'class' => 'button tiny') ); ?> 

<div class="related">
	<h3><?php echo __('Addresses'); ?></h3>
	<?php if (!empty($client['Address'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Address Line'); ?></th>
		<th><?php echo __('Postal Code'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('Address Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($client['Address'] as $address): ?>
		<tr>
			<td><?php echo $address['address_line']; ?></td>
			<td><?php echo $address['postal_code']; ?></td>
			<td><?php echo $address['city']; ?></td>
			<td><?php echo ($address['address_type'] == 1) ? "Primary": "Other"; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'addresses', 'action' => 'view', $address['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'addresses', 'action' => 'edit', $address['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'addresses', 'action' => 'delete', $address['id']), array(), __('Are you sure you want to delete # %s?', $address['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

<?php echo $this->Html->link(__('New Address'), array('controller' => 'addresses', 'action' => 'add'), array('class' => 'button tiny')); ?>