<div class="clientFlightSeats index">
	<h2><?php echo __('Client Flight Seats'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('client_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('flight_seat_id'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($clientFlightSeats as $clientFlightSeat): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($clientFlightSeat['Client']['id'], array('controller' => 'clients', 'action' => 'view', $clientFlightSeat['Client']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($clientFlightSeat['FlightSeat']['id'], array('controller' => 'flight_seats', 'action' => 'view', $clientFlightSeat['FlightSeat']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $clientFlightSeat['ClientFlightSeat']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $clientFlightSeat['ClientFlightSeat']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $clientFlightSeat['ClientFlightSeat']['id']), array(), __('Are you sure you want to delete # %s?', $clientFlightSeat['ClientFlightSeat']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Client Flight Seat'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
