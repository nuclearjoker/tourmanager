<div class="clientFlightSeats view">
<h2><?php echo __('Client Flight Seat'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($clientFlightSeat['ClientFlightSeat']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client'); ?></dt>
		<dd>
			<?php echo $this->Html->link($clientFlightSeat['Client']['id'], array('controller' => 'clients', 'action' => 'view', $clientFlightSeat['Client']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flight Seat'); ?></dt>
		<dd>
			<?php echo $this->Html->link($clientFlightSeat['FlightSeat']['id'], array('controller' => 'flight_seats', 'action' => 'view', $clientFlightSeat['FlightSeat']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Client Flight Seat'), array('action' => 'edit', $clientFlightSeat['ClientFlightSeat']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Client Flight Seat'), array('action' => 'delete', $clientFlightSeat['ClientFlightSeat']['id']), array(), __('Are you sure you want to delete # %s?', $clientFlightSeat['ClientFlightSeat']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Client Flight Seats'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client Flight Seat'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
