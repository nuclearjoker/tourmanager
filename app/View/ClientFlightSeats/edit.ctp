<div class="clientFlightSeats form">
<?php echo $this->Form->create('ClientFlightSeat'); ?>
	<fieldset>
		<legend><?php echo __('Edit Client Flight Seat'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_id');
		echo $this->Form->input('flight_seat_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ClientFlightSeat.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ClientFlightSeat.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Client Flight Seats'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('controller' => 'flight_seats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('controller' => 'flight_seats', 'action' => 'add')); ?> </li>
	</ul>
</div>
