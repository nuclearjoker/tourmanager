<div class="roomNumbers view">
<h2><?php echo __('Room Number'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($roomNumber['RoomNumber']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accomodation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($roomNumber['Accomodation']['id'], array('controller' => 'accomodations', 'action' => 'view', $roomNumber['Accomodation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Room Code'); ?></dt>
		<dd>
			<?php echo h($roomNumber['RoomNumber']['room_code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Room Number'), array('action' => 'edit', $roomNumber['RoomNumber']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Room Number'), array('action' => 'delete', $roomNumber['RoomNumber']['id']), array(), __('Are you sure you want to delete # %s?', $roomNumber['RoomNumber']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Room Numbers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Room Number'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accomodations'), array('controller' => 'accomodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accomodation'), array('controller' => 'accomodations', 'action' => 'add')); ?> </li>
	</ul>
</div>
