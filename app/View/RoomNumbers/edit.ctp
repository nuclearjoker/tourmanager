<div class="roomNumbers form">
<?php echo $this->Form->create('RoomNumber'); ?>
	<fieldset>
		<legend><?php echo __('Edit Room Number'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('accomodation_id');
		echo $this->Form->input('room_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RoomNumber.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('RoomNumber.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Room Numbers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Accomodations'), array('controller' => 'accomodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accomodation'), array('controller' => 'accomodations', 'action' => 'add')); ?> </li>
	</ul>
</div>
