<div class="flightSeats view">
<h2><?php echo __('Flight Seat'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($flightSeat['FlightSeat']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flight'); ?></dt>
		<dd>
			<?php echo $this->Html->link($flightSeat['Flight']['id'], array('controller' => 'flights', 'action' => 'view', $flightSeat['Flight']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seat Code'); ?></dt>
		<dd>
			<?php echo h($flightSeat['FlightSeat']['seat_code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Flight Seat'), array('action' => 'edit', $flightSeat['FlightSeat']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Flight Seat'), array('action' => 'delete', $flightSeat['FlightSeat']['id']), array(), __('Are you sure you want to delete # %s?', $flightSeat['FlightSeat']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight Seat'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flights'), array('controller' => 'flights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight'), array('controller' => 'flights', 'action' => 'add')); ?> </li>
	</ul>
</div>
