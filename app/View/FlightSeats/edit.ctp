<div class="flightSeats form">
<?php echo $this->Form->create('FlightSeat'); ?>
	<fieldset>
		<legend><?php echo __('Edit Flight Seat'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('flight_id');
		echo $this->Form->input('seat_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FlightSeat.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('FlightSeat.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Flight Seats'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Flights'), array('controller' => 'flights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flight'), array('controller' => 'flights', 'action' => 'add')); ?> </li>
	</ul>
</div>
