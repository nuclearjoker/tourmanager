<div class="tourOptions view">
<h2><?php echo __('Tour Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tourOption['TourOption']['id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tour Option'), array('action' => 'edit', $tourOption['TourOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tour Option'), array('action' => 'delete', $tourOption['TourOption']['id']), array(), __('Are you sure you want to delete # %s?', $tourOption['TourOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tour Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour Option'), array('action' => 'add')); ?> </li>
	</ul>
</div>
