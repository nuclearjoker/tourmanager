<div class="tourOptions form">
<?php echo $this->Form->create('TourOption'); ?>
	<fieldset>
		<legend><?php echo __('Edit Tour Option'); ?></legend>
	<?php
		echo $this->Form->input('id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TourOption.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('TourOption.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tour Options'), array('action' => 'index')); ?></li>
	</ul>
</div>
