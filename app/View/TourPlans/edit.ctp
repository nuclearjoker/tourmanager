<div class="tourPlans form">
<?php echo $this->Form->create('TourPlan'); ?>
	<fieldset>
		<legend><?php echo __('Edit Tour Plan'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('tour_id');
		echo $this->Form->input('main_tour');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TourPlan.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('TourPlan.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Tour Plans'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
