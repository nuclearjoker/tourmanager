
<?php echo $this->Form->create('TourPlan'); ?>
	<fieldset>
		<legend><?php echo __('Add Tour Plan'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('tour_id', array( 'default' => $tour_id ));
		echo $this->Form->input('main_tour', array('label' => 'Main Tour Plan') );
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

<?php echo $this->Html->link(__('List Tour Plans'), array('action' => 'index')); ?>
<?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?>
<?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?>
