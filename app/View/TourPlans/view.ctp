<div class="tourPlans view">
<h2><?php echo __('Tour Plan'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tourPlan['TourPlan']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tour'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tourPlan['Tour']['id'], array('controller' => 'tours', 'action' => 'view', $tourPlan['Tour']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Main Tour'); ?></dt>
		<dd>
			<?php echo h($tourPlan['TourPlan']['main_tour']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tour Plan'), array('action' => 'edit', $tourPlan['TourPlan']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tour Plan'), array('action' => 'delete', $tourPlan['TourPlan']['id']), array(), __('Are you sure you want to delete # %s?', $tourPlan['TourPlan']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tour Plans'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour Plan'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
