<div class="tourPlans index">
	<h2><?php echo __('Tour Plans'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('TourPlan.name'); ?></th>
    		<th><?php echo $this->Paginator->sort('Tour.name'); ?></th>
    		<th><?php echo $this->Paginator->sort('main_tour'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($tourPlans as $tourPlan): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($tourPlan['TourPlan']['name'], array('controller' => 'tours', 'action' => 'view', $tourPlan['Tour']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($tourPlan['Tour']['name'], array('controller' => 'tours', 'action' => 'view', $tourPlan['Tour']['id'])); ?>
		</td>
		<td><?php echo h(( $tourPlan['TourPlan']['main_tour'] == 1)? "Yes": "No"); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $tourPlan['TourPlan']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tourPlan['TourPlan']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tourPlan['TourPlan']['id']), array(), __('Are you sure you want to delete # %s?', $tourPlan['TourPlan']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Tour Plan'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
