<h2><?php echo __('Tour'); ?></h2>
	<dl>
		<dt><?php echo __('Tour Name'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date From'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['date_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date To'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['date_to']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->Html->link(__('Edit Tour'), array('action' => 'edit', $tour['Tour']['id']), array('class' => 'button tiny')); ?>
<?php echo $this->Form->postLink(__('Delete Tour'), array('action' => 'delete', $tour['Tour']['id']),
	array('class' => 'button tiny'),
	 __('Are you sure you want to delete %s?', $tour['Tour']['name'])); ?>
<?php echo $this->Html->link(__('New Tour'), array('action' => 'add'), array('class' => 'button tiny')); ?>
<?php echo $this->Html->link(__('List Tours'), array('action' => 'index'), array('class' => 'button tiny')); ?>

<div class="row">
	<h3><?php echo __('Related Flights'); ?></h3>
	<?php if (!empty($tour['Flight'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Landing Time'); ?></th>
		<th><?php echo __('Tour Id'); ?></th>
		<th><?php echo __('From City Id'); ?></th>
		<th><?php echo __('To City Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tour['Flight'] as $flight): ?>
		<tr>
			<td><?php echo $flight['departure_time']; ?></td>
			<td><?php echo $flight['landing_time']; ?></td>
			<td><?php echo $flight['tour_id']; ?></td>
			<td><?php echo $flight['from_city_id']; ?></td>
			<td><?php echo $flight['to_city_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'flights', 'action' => 'view', $flight['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'flights', 'action' => 'edit', $flight['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'flights', 'action' => 'delete', $flight['id']), array(), __('Are you sure you want to delete # %s?', $flight['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

<?php echo $this->Html->link(__('New Flight'), array('controller' => 'flights', 'action' => 'add', $tour["Tour"]["id"]), array('class' => 'button tiny')); ?>
</div>


<div class="row">
	<h3><?php echo __('Related Tour Plans'); ?></h3>
	<?php if (!empty($tour['TourPlan'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Tour Id'); ?></th>
		<th><?php echo __('Main Tour'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tour['TourPlan'] as $tourPlan): ?>
		<tr>
			<td><?php echo $tourPlan['id']; ?></td>
			<td><?php echo $tourPlan['tour_id']; ?></td>
			<td><?php echo $tourPlan['main_tour']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tour_plans', 'action' => 'view', $tourPlan['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tour_plans', 'action' => 'edit', $tourPlan['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tour_plans', 'action' => 'delete', $tourPlan['id']), array(), __('Are you sure you want to delete # %s?', $tourPlan['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
<?php echo $this->Html->link(__('New Tour Plan'), array('controller' => 'tour_plans', 'action' => 'add', $tour["Tour"]["id"]), array('class' => 'button tiny')); ?>
</div>
