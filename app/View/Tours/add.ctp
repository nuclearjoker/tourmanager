<script>
$(function() {
   $( ".datepick" ).datepicker( {  dateFormat: "yy-mm-dd", constrainInput: false });
   //$( ".datepick" ).Zebra_DatePicker( {  dateFormat: "yy-mm-dd", constrainInput: false  });
});
</script>
<?php echo $this->Form->create('Tour'); ?>
	<fieldset>
		<legend><?php echo __('Add Tour'); ?></legend>
	<?php
		echo $this->Form->input('parent_id', array( 'label' => 'Main tour', 'type' => 'select', 'options' => $tours ));
		echo $this->Form->input('name');
		echo $this->Form->input('date_from', array('type'=>'text', 'class'=>'datepick') );
		echo $this->Form->input('date_to', array('type'=>'text', 'class'=>'datepick') );
		echo $this->Form->input('price');
	?>
	</fieldset>
	<input type="submit" value="Add Tour" class="button tiny">
<?php echo $this->Form->end(); ?>