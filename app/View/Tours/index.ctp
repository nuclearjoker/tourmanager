<h2><?php echo __('Tours'); ?></h2>
<table cellpadding="0" cellspacing="0">
	<thead>
		<tr>
				<th><?php echo $this->Paginator->sort('name'); ?></th>
				<th><?php echo $this->Paginator->sort('date_from'); ?></th>
				<th><?php echo $this->Paginator->sort('date_to'); ?></th>
				<th><?php echo $this->Paginator->sort('price'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
	</thead>
<tbody>
<?php foreach ($tours as $tour): ?>
<tr>
	<td><?php echo h($tour['Tour']['name']); ?>&nbsp;</td>
	<td><?php echo h($tour['Tour']['date_from']); ?>&nbsp;</td>
	<td><?php echo h($tour['Tour']['date_to']); ?>&nbsp;</td>
	<td> <?php echo $tour['Tour']['price']; ?> </td>
	<td class="actions">
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $tour['Tour']['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tour['Tour']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tour['Tour']['id']), array(), __('Are you sure you want to delete  %s?', $tour['Tour']['name'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php echo $this->element('pagination'); ?>
<?php echo $this->Html->link(__('New Tour'), array('action' => 'add'), array('class' => 'button tiny')); ?>