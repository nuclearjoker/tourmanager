<div class="supplierCategories form">
<?php echo $this->Form->create('SupplierCategory'); ?>
	<fieldset>
		<legend><?php echo __('Edit Supplier Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SupplierCategory.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SupplierCategory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Supplier Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
