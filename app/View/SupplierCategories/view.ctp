<div class="supplierCategories view">
<h2><?php echo __('Supplier Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($supplierCategory['SupplierCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($supplierCategory['SupplierCategory']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Supplier Category'), array('action' => 'edit', $supplierCategory['SupplierCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Supplier Category'), array('action' => 'delete', $supplierCategory['SupplierCategory']['id']), array(), __('Are you sure you want to delete # %s?', $supplierCategory['SupplierCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplier Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Suppliers'); ?></h3>
	<?php if (!empty($supplierCategory['Supplier'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Supplier Type Id'); ?></th>
		<th><?php echo __('Alternative Name'); ?></th>
		<th><?php echo __('Short Code'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Contact Person'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($supplierCategory['Supplier'] as $supplier): ?>
		<tr>
			<td><?php echo $supplier['id']; ?></td>
			<td><?php echo $supplier['supplier_type_id']; ?></td>
			<td><?php echo $supplier['alternative_name']; ?></td>
			<td><?php echo $supplier['short_code']; ?></td>
			<td><?php echo $supplier['address']; ?></td>
			<td><?php echo $supplier['city_id']; ?></td>
			<td><?php echo $supplier['email']; ?></td>
			<td><?php echo $supplier['contact_person']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'suppliers', 'action' => 'view', $supplier['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'suppliers', 'action' => 'edit', $supplier['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'suppliers', 'action' => 'delete', $supplier['id']), array(), __('Are you sure you want to delete # %s?', $supplier['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo ->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
