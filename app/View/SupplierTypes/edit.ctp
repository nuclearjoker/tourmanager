<div class="supplierTypes form">
<?php echo $this->Form->create('SupplierType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Supplier Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('supplier_category_id');
		echo $this->Form->input('name');
		echo $this->Form->input('address');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SupplierType.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SupplierType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Supplier Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
