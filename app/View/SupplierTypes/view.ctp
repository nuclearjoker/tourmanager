<div class="supplierTypes view">
<h2><?php echo __('Supplier Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($supplierType['SupplierType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supplier Category Id'); ?></dt>
		<dd>
			<?php echo h($supplierType['SupplierType']['supplier_category_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($supplierType['SupplierType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($supplierType['SupplierType']['address']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Supplier Type'), array('action' => 'edit', $supplierType['SupplierType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Supplier Type'), array('action' => 'delete', $supplierType['SupplierType']['id']), array(), __('Are you sure you want to delete # %s?', $supplierType['SupplierType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplier Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Suppliers'), array('controller' => 'suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Suppliers'); ?></h3>
	<?php if (!empty($supplierType['Supplier'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Supplier Type Id'); ?></th>
		<th><?php echo __('Alternative Name'); ?></th>
		<th><?php echo __('Short Code'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Contact Person'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($supplierType['Supplier'] as $supplier): ?>
		<tr>
			<td><?php echo $supplier['id']; ?></td>
			<td><?php echo $supplier['supplier_type_id']; ?></td>
			<td><?php echo $supplier['alternative_name']; ?></td>
			<td><?php echo $supplier['short_code']; ?></td>
			<td><?php echo $supplier['address']; ?></td>
			<td><?php echo $supplier['city_id']; ?></td>
			<td><?php echo $supplier['email']; ?></td>
			<td><?php echo $supplier['contact_person']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'suppliers', 'action' => 'view', $supplier['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'suppliers', 'action' => 'edit', $supplier['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'suppliers', 'action' => 'delete', $supplier['id']), array(), __('Are you sure you want to delete # %s?', $supplier['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo ->link(__('New Supplier'), array('controller' => 'suppliers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
