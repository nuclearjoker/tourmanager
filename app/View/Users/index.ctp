
<h2><?php echo __('Users'); ?></h2>
<table cellpadding="0" cellspacing="0">
<thead>
<tr>
		<th><?php echo $this->Paginator->sort('username'); ?></th>
		<th><?php echo $this->Paginator->sort('role'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
</tr>
</thead>
<tbody>
<?php foreach ($users as $user): ?>
<tr>
	<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
	<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
	<td class="actions">
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

<?php echo $this->element('pagination'); ?>

<?php echo $this->Html->link(__('New User'), array('action' => 'add'), array('class' => 'tiny button')); ?>
