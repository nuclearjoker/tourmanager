<?php echo $this->Form->create('Address'); ?>
<fieldset>
	<legend><?php echo __('Add Address'); ?></legend>
<?php
	echo $this->Form->input('address_line');
	echo $this->Form->input('postal_code');
	echo $this->Form->input('city_id');
	echo $this->Form->input('address_type');
?>
</fieldset>
<input type="submit" value="Add Address" class="button tiny">
<?php echo $this->Form->end(); ?>