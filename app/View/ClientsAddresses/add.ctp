<div class="clientsAddresses form">
<?php echo $this->Form->create('ClientsAddress'); ?>
	<fieldset>
		<legend><?php echo __('Add Clients Address'); ?></legend>
	<?php
		echo $this->Form->input('clients_id');
		echo $this->Form->input('addresses_id');
		echo $this->Form->input('relationship');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('List Clients Addresses'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Clients'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses'), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Addresses'), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>
