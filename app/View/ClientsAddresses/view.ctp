<div class="clientsAddresses view">
<h2><?php echo __('Clients Address'); ?></h2>
	<dl>
		<dt><?php echo __('Clients'); ?></dt>
		<dd>
			<?php echo $this->Html->link($clientsAddress['Clients']['id'], array('controller' => 'clients', 'action' => 'view', $clientsAddress['Clients']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Addresses'); ?></dt>
		<dd>
			<?php echo $this->Html->link($clientsAddress['Addresses']['id'], array('controller' => 'addresses', 'action' => 'view', $clientsAddress['Addresses']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Relationship'); ?></dt>
		<dd>
			<?php echo h($clientsAddress['ClientsAddress']['relationship']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($clientsAddress['ClientsAddress']['id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Clients Address'), array('action' => 'edit', $clientsAddress['ClientsAddress']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Clients Address'), array('action' => 'delete', $clientsAddress['ClientsAddress']['id']), array(), __('Are you sure you want to delete # %s?', $clientsAddress['ClientsAddress']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients Addresses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Clients Address'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Clients'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses'), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Addresses'), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>
