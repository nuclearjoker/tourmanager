<div class="clientsAddresses index">
	<h2><?php echo __('Clients Addresses'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
    		<th><?php echo $this->Paginator->sort('clients_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('addresses_id'); ?></th>
    		<th><?php echo $this->Paginator->sort('relationship'); ?></th>
    		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($clientsAddresses as $clientsAddress): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($clientsAddress['Clients']['id'], array('controller' => 'clients', 'action' => 'view', $clientsAddress['Clients']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($clientsAddress['Addresses']['id'], array('controller' => 'addresses', 'action' => 'view', $clientsAddress['Addresses']['id'])); ?>
		</td>
		<td><?php echo h($clientsAddress['ClientsAddress']['relationship']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $clientsAddress['ClientsAddress']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $clientsAddress['ClientsAddress']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $clientsAddress['ClientsAddress']['id']), array(), __('Are you sure you want to delete # %s?', $clientsAddress['ClientsAddress']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Clients Address'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Clients'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses'), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Addresses'), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
	</ul>
</div>
