<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="#">Christine van Wyk-Toere</a></h1>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="<?php echo $this->Html->link( array( 'controller' => 'tours', 'action' => 'index' )); ?>" ><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
    <ul class="right">
      <li class="has-form">
        <div class="row collapse">
          <div class="small-8 large-7 columns">
             <input type="text" placeholder="Search" id="main-search" target="/searchers/search/{query}.json">
          </div>
        </div>
      </li>
      <li><?php echo $this->Html->link(__('Tours'), array( 'controller' => 'tours', 'action' => 'index')); ?></li>
      <li><?php echo $this->Html->link(__('Clients'), array( 'controller' => 'clients', 'action' => 'index')); ?></li>
      <li><?php echo $this->Html->link(__('Suppliers'), array( 'controller' => 'suppliers', 'action' => 'index')); ?></li>
      <li class="has-dropdown">
        <a href="#"> Admin </a>
        <ul class="dropdown">
          <li><a href="#"> </a></li>
          <li><a href="#"> Reports </a></li>
          <li><?php echo $this->Html->link(__('Users'), array( 'controller' => 'users', 'action' => 'index')); ?></li>
          <li><?php echo $this->Html->link(__('Cities'), array( 'controller' => 'cities', 'action' => 'index')); ?></li>
        </ul>
      </li>
    </ul>

  </section>
</nav>

<script>
$(document).ready(function () {
    $("#main-search").autocomplete({
        source: function(request, response) {
            $.getJSON($("#main-search").attr('target').replace('{query}', request.term), null, response);
        },
        delay: 100,
        select: function(event, ui) {
            var id = 0;
            if ((id = ui.item.id)) {
                location.href = ui.item.url;
            }
            return false;
        }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a href='" + item.url + "'>" + item.first_name+","+item.last_name + "</a>")
            .appendTo(ul);
    };
});
</script>