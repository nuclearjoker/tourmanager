<div class="row">
	<div class="small-12 columns pagination-centered">
		<ul class="pagination">
		  <?php
		  	echo $this->Paginator->prev('<<',
		  		array(
		  			'tag'=>'li',
		  			'disabledTag'=>'a'
		  		));
		  	echo $this->Paginator->numbers(array(
		  			'tag'=>'li',
		  			'separator'=>'',
		  			'currentTag'=>'a'
		  		));
		  	echo $this->Paginator->next('>>',
		  		array(
		  			'tag'=>'li',
		  			'disabledTag'=>'a'
		  		));
		  ?>
		</ul>
	</div>
</div>