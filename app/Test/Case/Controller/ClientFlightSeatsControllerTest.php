<?php
App::uses('ClientFlightSeatsController', 'Controller');

/**
 * ClientFlightSeatsController Test Case
 *
 */
class ClientFlightSeatsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client_flight_seat',
		'app.client',
		'app.address',
		'app.clients_address',
		'app.flight_seat',
		'app.flight',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.tour_plan',
		'app.from_city',
		'app.to_city'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
