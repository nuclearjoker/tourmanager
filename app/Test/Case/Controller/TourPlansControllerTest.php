<?php
App::uses('TourPlansController', 'Controller');

/**
 * TourPlansController Test Case
 *
 */
class TourPlansControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tour_plan',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.flight',
		'app.from_city',
		'app.to_city',
		'app.flight_seat'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
