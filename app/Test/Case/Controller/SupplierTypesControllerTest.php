<?php
App::uses('SupplierTypesController', 'Controller');

/**
 * SupplierTypesController Test Case
 *
 */
class SupplierTypesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.supplier_type',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier',
		'app.supplier_category',
		'app.tour',
		'app.flight',
		'app.from_city',
		'app.to_city',
		'app.flight_seat',
		'app.tour_plan'
	);

}
