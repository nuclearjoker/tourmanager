<?php
App::uses('SupplierCategoriesController', 'Controller');

/**
 * SupplierCategoriesController Test Case
 *
 */
class SupplierCategoriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.supplier_category',
		'app.supplier',
		'app.supplier_type',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.tour',
		'app.flight',
		'app.from_city',
		'app.to_city',
		'app.flight_seat',
		'app.tour_plan'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
