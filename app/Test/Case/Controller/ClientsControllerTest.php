<?php
App::uses('ClientsController', 'Controller');

/**
 * ClientsController Test Case
 *
 */
class ClientsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client',
		'app.client_flight_seat',
		'app.flight_seat',
		'app.flight',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.tour_plan',
		'app.from_city',
		'app.to_city',
		'app.address',
		'app.addresses_client'
	);

}
