<?php
App::uses('RoomNumber', 'Model');

/**
 * RoomNumber Test Case
 *
 */
class RoomNumberTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.room_number',
		'app.accomodation',
		'app.city',
		'app.country',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.tour',
		'app.flight',
		'app.flight_seat',
		'app.tour_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RoomNumber = ClassRegistry::init('RoomNumber');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RoomNumber);

		parent::tearDown();
	}

}
