<?php
App::uses('SupplierCategory', 'Model');

/**
 * SupplierCategory Test Case
 *
 */
class SupplierCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.supplier_category',
		'app.supplier_type',
		'app.supplier',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.tour',
		'app.flight',
		'app.from_city',
		'app.to_city',
		'app.flight_seat',
		'app.tour_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SupplierCategory = ClassRegistry::init('SupplierCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SupplierCategory);

		parent::tearDown();
	}

}
