<?php
App::uses('TourPlan', 'Model');

/**
 * TourPlan Test Case
 *
 */
class TourPlanTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tour_plan',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.flight',
		'app.flight_seat'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TourPlan = ClassRegistry::init('TourPlan');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TourPlan);

		parent::tearDown();
	}

}
