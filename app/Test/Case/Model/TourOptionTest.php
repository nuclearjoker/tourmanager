<?php
App::uses('TourOption', 'Model');

/**
 * TourOption Test Case
 *
 */
class TourOptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tour_option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TourOption = ClassRegistry::init('TourOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TourOption);

		parent::tearDown();
	}

}
