<?php
App::uses('Accomodation', 'Model');

/**
 * Accomodation Test Case
 *
 */
class AccomodationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accomodation',
		'app.city',
		'app.country',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.tour',
		'app.flight',
		'app.from_city',
		'app.to_city',
		'app.flight_seat',
		'app.tour_plan',
		'app.room_number'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Accomodation = ClassRegistry::init('Accomodation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Accomodation);

		parent::tearDown();
	}

}
