<?php
App::uses('AddressesClient', 'Model');

/**
 * AddressesClient Test Case
 *
 */
class AddressesClientTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.addresses_client',
		'app.clients',
		'app.addresses'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AddressesClient = ClassRegistry::init('AddressesClient');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AddressesClient);

		parent::tearDown();
	}

}
