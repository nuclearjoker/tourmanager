<?php
App::uses('TourFlight', 'Model');

/**
 * TourFlight Test Case
 *
 */
class TourFlightTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tour_flight',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier_category',
		'app.supplier',
		'app.flight',
		'app.flight_seat',
		'app.tour_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TourFlight = ClassRegistry::init('TourFlight');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TourFlight);

		parent::tearDown();
	}

}
