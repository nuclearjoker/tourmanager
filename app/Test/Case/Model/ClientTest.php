<?php
App::uses('Client', 'Model');

/**
 * Client Test Case
 *
 */
class ClientTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client',
		'app.client_flight_seat',
		'app.flight_seat',
		'app.flight',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier_category',
		'app.supplier',
		'app.address',
		'app.addresses_client'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Client = ClassRegistry::init('Client');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Client);

		parent::tearDown();
	}

}
