<?php
App::uses('ClientFlightSeat', 'Model');

/**
 * ClientFlightSeat Test Case
 *
 */
class ClientFlightSeatTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client_flight_seat',
		'app.client',
		'app.address',
		'app.clients_address',
		'app.flight_seat',
		'app.flight',
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.tour_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClientFlightSeat = ClassRegistry::init('ClientFlightSeat');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClientFlightSeat);

		parent::tearDown();
	}

}
