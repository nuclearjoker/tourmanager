<?php
App::uses('Tour', 'Model');

/**
 * Tour Test Case
 *
 */
class TourTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tour',
		'app.city',
		'app.country',
		'app.accomodation',
		'app.room_number',
		'app.supplier_type',
		'app.supplier',
		'app.supplier_category',
		'app.flight',
		'app.flight_seat',
		'app.tour_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tour = ClassRegistry::init('Tour');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tour);

		parent::tearDown();
	}

}
