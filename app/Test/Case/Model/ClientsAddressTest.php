<?php
App::uses('ClientsAddress', 'Model');

/**
 * ClientsAddress Test Case
 *
 */
class ClientsAddressTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.clients_address',
		'app.clients',
		'app.addresses'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClientsAddress = ClassRegistry::init('ClientsAddress');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClientsAddress);

		parent::tearDown();
	}

}
