<?php
/**
 * FlightFixture
 *
 */
class FlightFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'departure_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'landing_time' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'tour_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'from_city_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'to_city_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'tour_id_idx' => array('column' => 'tour_id', 'unique' => 0),
			'from_city_id_idx' => array('column' => 'from_city_id', 'unique' => 0),
			'to_city_id_idx' => array('column' => 'to_city_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'departure_time' => '2014-09-07 19:52:10',
			'landing_time' => 'Lorem ipsum dolor sit amet',
			'tour_id' => 1,
			'from_city_id' => 1,
			'to_city_id' => 1
		),
	);

}
