<?php
/**
 * AddressesClientFixture
 *
 */
class AddressesClientFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'clients_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'addresses_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'relationship' => array('type' => 'string', 'null' => true, 'default' => 'PRIMARY', 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'clients_id' => 1,
			'addresses_id' => 1,
			'relationship' => 'Lorem ipsum dolor sit amet'
		),
	);

}
