<?php
/**
 * ClientsAddressFixture
 *
 */
class ClientsAddressFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'clients_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'addresses_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'relationship' => array('type' => 'string', 'null' => true, 'default' => 'PRIMARY', 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'clients_id' => 1,
			'addresses_id' => 1,
			'relationship' => 'Lorem ipsum dolor sit amet',
			'id' => 1
		),
	);

}
