<?php
App::uses('AppModel', 'Model');
/**
 * ClientFlightSeat Model
 *
 * @property Client $Client
 * @property FlightSeat $FlightSeat
 */
class ClientFlightSeat extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FlightSeat' => array(
			'className' => 'FlightSeat',
			'foreignKey' => 'flight_seat_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
