<?php
App::uses('AppModel', 'Model');
/**
 * Tour Model
 *
 */
class Tour extends AppModel {
  	public $actsAs = array(
  		'Tree'	,
		'ElasticSearchIndex.ElasticSearchIndexable' => array(
        )
   );

/**
 * Display field
 */
	public $displayField = 'name';

}
