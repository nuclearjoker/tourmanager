<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property ClientFlightSeat $ClientFlightSeat
 * @property Address $Address
 */
class Client extends AppModel {
    public $actsAs = array(
        'ElasticSearchIndex.ElasticSearchIndexable' => array(),
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ClientFlightSeat' => array(
			'className' => 'ClientFlightSeat',
			'foreignKey' => 'client_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Address' => array(
			'className' => 'Address',
			'joinTable' => 'addresses_clients',
			'foreignKey' => 'client_id',
			'associationForeignKey' => 'address_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
