<?php
App::uses('AppModel', 'Model');
/**
 * AddressesClient Model
 *
 * @property Clients $Clients
 * @property Addresses $Addresses
 */
class AddressesClient extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Clients' => array(
			'className' => 'Clients',
			'foreignKey' => 'clients_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Addresses' => array(
			'className' => 'Addresses',
			'foreignKey' => 'addresses_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
