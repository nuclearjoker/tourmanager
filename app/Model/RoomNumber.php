<?php
App::uses('AppModel', 'Model');
/**
 * RoomNumber Model
 *
 * @property Accomodation $Accomodation
 */
class RoomNumber extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Accomodation' => array(
			'className' => 'Accomodation',
			'foreignKey' => 'accomodation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
