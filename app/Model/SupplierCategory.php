<?php
App::uses('AppModel', 'Model');
/**
 * SupplierCategory Model
 *
 * @property SupplierType $SupplierType
 */
class SupplierCategory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'supplier_categories';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SupplierType' => array(
			'className' => 'SupplierType',
			'foreignKey' => 'supplier_category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
