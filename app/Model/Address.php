<?php
App::uses('AppModel', 'Model');
/**
 * Address Model
 *
 * @property Client $Client
 */
class Address extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Client' => array(
			'className' => 'Client',
			'joinTable' => 'addresses_clients',
			'foreignKey' => 'address_id',
			'associationForeignKey' => 'client_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
