<?php
App::uses('AppModel', 'Model');
/**
 * Supplier Model
 *
 * @property SupplierType $SupplierType
 * @property City $City
 */
class Supplier extends AppModel {
	public $actsAs = array(
        'ElasticSearchIndex.ElasticSearchIndexable' => array(
		   	'rebuildOnUpdate' => true,
		   	'fields' => 'name',
		   	'limit' => 10
        ),
    );


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 * @var array
 */
	public $belongsTo = array(
		'SupplierType' => array(
			'className' => 'SupplierType',
			'foreignKey' => 'supplier_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
