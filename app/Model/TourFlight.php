<?php
App::uses('AppModel', 'Model');
/**
 * TourFlight Model
 *
 * @property Tour $Tour
 * @property Flight $Flight
 */
class TourFlight extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tour' => array(
			'className' => 'Tour',
			'foreignKey' => 'tour_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Flight' => array(
			'className' => 'Flight',
			'foreignKey' => 'flight_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
