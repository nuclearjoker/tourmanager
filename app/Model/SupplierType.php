<?php
App::uses('AppModel', 'Model');
/**
 * SupplierType Model
 *
 * @property SupplierCategory $SupplierCategory
 * @property Supplier $Supplier
 */
class SupplierType extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'supplier_type';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SupplierCategory' => array(
			'className' => 'SupplierCategory',
			'foreignKey' => 'supplier_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Supplier' => array(
			'className' => 'Supplier',
			'foreignKey' => 'supplier_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
