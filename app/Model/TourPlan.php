<?php
App::uses('AppModel', 'Model');
/**
 * TourPlan Model
 *
 * @property Tour $Tour
 */
class TourPlan extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	// public $useTable = 'tour_plan';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tour' => array(
			'className' => 'Tour',
			'foreignKey' => 'tour_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
