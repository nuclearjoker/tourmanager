<?php
App::uses('AppModel', 'Model');

class Flight extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Tour' => array(
			'className' => 'Tour',
			'foreignKey' => 'tour_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FromCity' => array(
			'className' => 'City',
			'foreignKey' => 'from_city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ToCity' => array(
			'className' => 'City',
			'foreignKey' => 'to_city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'FlightSeat' => array(
			'className' => 'FlightSeat',
			'foreignKey' => 'flight_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
