SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `cvwt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `cvwt` ;

-- -----------------------------------------------------
-- Table `cvwt`.`clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`clients` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NULL,
  `id_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`addresses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`addresses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address_line` VARCHAR(45) NULL,
  `postal_code` VARCHAR(10) NULL,
  `city` VARCHAR(45) NULL,
  `address_type` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`tours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`tours` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tour_name` VARCHAR(255) NULL,
  `city_id` INT NULL,
  `date_from` INT NULL,
  `date_to` INT NULL,
  `parent_id` INT NULL,
  `breakaway` TINYINT(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`countries` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`cities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country_id` INT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`tour_plan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`tour_plan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tour_id` VARCHAR(45) NULL,
  `main_tour` TINYINT(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`addresses_clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`addresses_clients` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clients_id` INT NULL,
  `addresses_id` INT NULL,
  `relationship` VARCHAR(45) NULL DEFAULT 'PRIMARY',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`flights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`flights` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `departure_time` DATETIME NULL,
  `landing_time` VARCHAR(45) NULL,
  `tour_id` INT NULL,
  `from_city_id` INT NULL,
  `to_city_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `tour_id_idx` (`tour_id` ASC),
  INDEX `from_city_id_idx` (`from_city_id` ASC),
  INDEX `to_city_id_idx` (`to_city_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`address_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`address_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address_type` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`suppliers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`suppliers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `supplier_type_id` INT NULL,
  `name` VARCHAR(255) NULL,
  `alternative_name` VARCHAR(255) NULL,
  `short_code` VARCHAR(10) NULL,
  `address` VARCHAR(1024) NULL,
  `city_id` INT NULL,
  `email` VARCHAR(255) NULL,
  `contact_person` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`room_numbers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`room_numbers` (
  `id` INT NULL AUTO_INCREMENT,
  `accomodation_id` INT NULL,
  `room_code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`tour_options`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`tour_options` (
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`supplier_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`supplier_categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`supplier_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`supplier_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `supplier_category_id` INT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`client_flight_seats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`client_flight_seats` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_id` INT NULL,
  `flight_seat_id` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`flight_seats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`flight_seats` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `flight_id` INT NULL,
  `seat_code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `flight_id_idx` (`flight_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`accomodation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`accomodation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city_id` INT NULL,
  `address` VARCHAR(1024) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvwt`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cvwt`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `role` VARCHAR(20) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
